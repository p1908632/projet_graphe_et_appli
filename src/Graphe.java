package src;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import javax.print.attribute.standard.Destination;

public class Graphe {

    private String name;
    private boolean oriente;
    private int nbSommets;
    private int nbArcs;
    private int nbValeursParSommet;
    private int nbValeursParArc;
    private ArrayList<Vertex> listeSommets;
    private ArrayList<Double> listeValeurSommets;
    private final ArrayList<LinkedList<Edge>> listeAdjacence;

    private int[] listeDegresInterieurs;     //d-
    private int[] listeDegresExterieurs;     //d+
    private int[] listeDegresTotaux;         //d
    
    private ArrayList<String> villesNonConnexes = new ArrayList<>() {{
    	add("saint-flour-15");
    	add("venarey-les-laumes");
    	add("montbard");
    	add("chatillon-sur-seine");
    	add("semur-en-auxois");
    	add("saulieu");
    	add("bagneres-de-luchon");
    	add("langogne");
    	add("chateau-chinon-(ville)");
    	add("clamecy-58");
    	add("gray");
    	add("arc-les-gray");
    	add("avallon");
    }};


    public Graphe(String filePath)
    {
        listeSommets = new ArrayList<Vertex>();
        listeAdjacence = new ArrayList<LinkedList<Edge>>();

        //Récupère les données du graphe depuis son fichier
        getGrapheFromFile(filePath);

        //Calcul des degrés totaux
        for (int i = 0; i < getNbSommets(); i++) {
            getListeDegresTotaux()[i] = getListeDegresExterieurs()[i] + getListeDegresInterieurs()[i];
        }
    }

    /**
     * Initialisation de la liste d'adjacence selon le nombre de sommets du graphe
     */
    public void initializeListeAdjacence()
    {
        for (int i = 0; i < getNbSommets(); i++) {
            getListeAdjacence().add(new LinkedList<Edge>());
        }
    }

    /**
     * Initialisation des listes de degrés des sommets du graphe
     */
    public void initializeListesDegres()
    {
        //initialisation automatique à 0 garantie par Java
        setListeDegresInterieurs(new int[getNbSommets()]);
        setListeDegresExterieurs(new int[getNbSommets()]);
        setListeDegresTotaux(new int[getNbSommets()]);
    }

    /**
     * Calcule une HashMap d'heuristiques par rapport à une ville passée en paramètres
     * @param arrivee Nom de la ville
     * @return La HashMap créée
     */
    public HashMap<String, Double> initializeHeuristiques(String arrivee)
    {
        HashMap<String, Double> heuristiques = new HashMap<>();
        Vertex vArrivee = findVertexByName(arrivee);

        double[] tabA, tabB;
        double dLambda, distanceS_A_B;
        for (Vertex v : this.getListeSommets()) {
            tabA = v.getValeursSommet();
            tabB = vArrivee.getValeursSommet();

            dLambda = Math.toRadians(tabB[0]) - Math.toRadians(tabA[0]);

            distanceS_A_B = Math.sin(Math.toRadians(tabA[1])) * Math.sin(Math.toRadians(tabB[1]))
                    + Math.cos(Math.toRadians(tabA[1])) * Math.cos(Math.toRadians(tabB[1]))
                    * Math.cos(dLambda);

            heuristiques.put(v.getNom(), Math.acos(distanceS_A_B) * 6378.137);
        }
        return heuristiques;
    }

    /////////////////////////
    /// Getters & Setters ///
    /////////////////////////

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean isOriente()
    {
        return oriente;
    }

    public void setOriente(boolean oriente)
    {
        this.oriente = oriente;
    }

    public int getNbSommets()
    {
        return nbSommets;
    }

    public void setNbSommets(int nbSommets)
    {
        this.nbSommets = nbSommets;
    }

    public int getNbValeursParSommet()
    {
        return nbValeursParSommet;
    }

    public void setNbValeursParSommet(int nbValeursParSommet)
    {
        this.nbValeursParSommet = nbValeursParSommet;
    }

    public int getNbArcs()
    {
        return nbArcs;
    }

    public void setNbArcs(int nbArcs)
    {
        this.nbArcs = nbArcs;
    }

    public int getNbValeursParArc()
    {
        return nbValeursParArc;
    }

    public void setNbValeursParArc(int nbValeursParArc)
    {
        this.nbValeursParArc = nbValeursParArc;
    }

    public ArrayList<Vertex> getListeSommets()
    {
        return listeSommets;
    }

    /**
     * Formate l'affichage des sommets
     * @return La chaîne formatée
     */
    public String getListeSommetsToString()
    {
        String res = "\n";
        for (int i = 0; i < getListeSommets().size(); i++) {
            Vertex sommet = getListeSommets().get(i);
            res += "   " + sommet.getNom() + " ";

            if (nbValeursParSommet > 0) res += sommet.getValeursSommetToString();
            res += getDegresSommetToString(sommet.getIndex());

            //Sauter le dernier élément
            if (i < getListeAdjacence().size() - 1) res += "\n";
        }
        return res;
    }

    public void setListeSommets(ArrayList<Vertex> listeSommets)
    {
        this.listeSommets = listeSommets;
    }

    public int[] getListeDegresInterieurs()
    {
        return listeDegresInterieurs;
    }

    public void setListeDegresInterieurs(int[] listeDegresInterieurs)
    {
        this.listeDegresInterieurs = listeDegresInterieurs;
    }

    public int[] getListeDegresExterieurs()
    {
        return listeDegresExterieurs;
    }

    public void setListeDegresExterieurs(int[] listeDegresExterieurs)
    {
        this.listeDegresExterieurs = listeDegresExterieurs;
    }

    public int[] getListeDegresTotaux()
    {
        return listeDegresTotaux;
    }

    /**
     * Formate l'affichage des degrés des sommets
     * @return La chaîne formatée
     */
    public String getDegresSommetToString(int index)
    {
        String res = "=> (";
        if (isOriente()) {
            res += "d+ = " + getListeDegresExterieurs()[index] + "; ";
            res += "d- = " + getListeDegresInterieurs()[index] + "; ";
        }
        res += "d = " + getListeDegresTotaux()[index];
        res += ") ";
        return res;
    }

    public void setListeDegresTotaux(int[] listeDegresTotaux)
    {
        this.listeDegresTotaux = listeDegresTotaux;
    }

    public ArrayList<LinkedList<Edge>> getListeAdjacence()
    {
        return listeAdjacence;
    }

    /**
     * Formate l'affichage de la liste d'adjacence du graphe
     * @return La chaîne formatée
     */
    public String getListeAdjacenceToString() {
        String res = "\n";

        for (int i = 0; i < getListeAdjacence().size(); i++) {
            LinkedList<Edge> list = getListeAdjacence().get(i);

            res += "   " + i + " : ";
            for (Edge arc : list) {
                res += arc.getSommetTerminal() + arc.getValeursToString();
            }

            //Sauter le dernier élément
            if (i < getListeAdjacence().size() - 1) res += "\n";
        }

        return res;
    }

    public ArrayList<Double> getListeValeurSommets()
    {
        return listeValeurSommets;
    }

    public void setListeValeurSommets(ArrayList<Double> listeValeurSommets)
    {
        this.listeValeurSommets = listeValeurSommets;
    }


    /////////////////////////
    /////// Parcours ////////
    /////////////////////////


    /**
     * Effectue un parcours en largeur sur le graphe en paramètre depuis un sommet
     * @param g Graphe à parcourir
     * @param a Sommet de départ
     * @return L'ArrayList de l'ordre des sommets parcourus
     */
    public static ArrayList<String> parcoursLargeur(Graphe g, Vertex a)
    {
        //p => algo du cours
        //ordreTraitement => ordonne directement les sommets

        ArrayList<Vertex> F = new ArrayList<Vertex>();  //simule une file
        HashMap<Integer, Boolean> marquer = new HashMap<Integer, Boolean>();
        //HashMap<String, Integer> traiter = new HashMap<String, Integer>();
        ArrayList<String> ordreTraitement = new ArrayList<String>();

        for (Vertex x : g.getListeSommets()) {
            marquer.put(x.getIndex(), false);
            //traiter.put(x.getNom(), 0);
        }
        marquer.put(a.getIndex(), true);
        //int p = 1;
        F.add(a);

        while (F.size() != 0) {
            Vertex x = F.get(0);

            for (Edge y : g.getListeAdjacence().get(x.getIndex())) {
                if (!marquer.get(y.getSommetTerminal())) {
                    marquer.put(y.getSommetTerminal(), true);
                    F.add(g.getListeSommets().get(y.getSommetTerminal()));
                }
            }

            //traiter.put(x.getNom(), p);
            ordreTraitement.add(x.getNom());
            F.remove(0);
            //p += 1;
        }
        return ordreTraitement;
    }

    /**
     * Méthode appelant un parcours en largeur et formatant son affichage
     * @param a Sommet de départ du parcours
     * @return Une chaîne représentant le parcours effectué
     */
    public String displayParcoursLargeur(Vertex a)
    {
        ArrayList<String> parcours = Graphe.parcoursLargeur(this, a);
        String res = "Parcours en Largeur de " + getName() + " : ";
        for (String sommet : parcours) {
            res += sommet + " ; ";
        }
        return res;
    }

    /**
     * Effectue un parcoursdu plus court chemin sur le graphe en paramètre depuis un sommet
     * @param g Graphe à parcourir
     * @param a Sommet de départ
     * @return L'ArrayList de l'ordre des sommets parcourus
     */
    public static HashMap<String, Integer> plusCourtChemin(Graphe g, Vertex a)
    {
        ArrayList<Vertex> F = new ArrayList<Vertex>();  //simule une file
        HashMap<Integer, Boolean> marquer = new HashMap<Integer, Boolean>();
        HashMap<String, Integer> traiter = new HashMap<String, Integer>();

        for (Vertex x : g.getListeSommets()) {
            marquer.put(x.getIndex(), false);
            traiter.put(x.getNom(), -1);    // => distance de +infini
        }
        marquer.put(a.getIndex(), true);
        F.add(a);
        traiter.put(a.getNom(), 0); //initialiser le sommet initial à 0

        while (F.size() != 0) {
            Vertex x = F.get(0);

            for (Edge y : g.getListeAdjacence().get(x.getIndex())) {
                if (!marquer.get(y.getSommetTerminal())) {
                    marquer.put(y.getSommetTerminal(), true);
                    Vertex current = g.getListeSommets().get(y.getSommetTerminal());
                    //distance du successeur : distance parent +1
                    traiter.put(current.getNom(), traiter.get(x.getNom()) + 1);
                    F.add(current);
                }
            }

            F.remove(0);
        }
        return traiter;
    }

    /**
     * Méthode appelant un calcul du plus cours chemin et formatant son affichage
     * @param a Sommet de départ du parcours
     * @return Une chaîne représentant le parcours effectué
     */
    public String displayPlusCourtChemin(Vertex a)
    {
        HashMap<String, Integer> longueurs = Graphe.plusCourtChemin(this, a);
        String res = "Longuers des chemins depuis " + a.getNom() + " de " + getName() + " :\n";

        for (Map.Entry<String, Integer> entry : longueurs.entrySet()) {
            res += entry.getKey() + " => " + entry.getValue() + "\n";
        }

        return res;
    }

    /**
     * Effectue un parcours en profondeur sur le graphe en paramètre depuis un sommet
     * @param g Graphe à parcourir
     * @param a Sommet de départ
     * @return L'ArrayList de l'ordre des sommets parcourus
     */
    public static ArrayList<String> parcoursProfondeur(Graphe g, Vertex a)
    {
        //p => algo du cours
        //ordreTraitement => ordonne directement les sommets

        ArrayList<Vertex> P = new ArrayList<Vertex>();  //simule une pile
        HashMap<Integer, Boolean> marquer = new HashMap<Integer, Boolean>();
        //HashMap<String, Integer> traiter = new HashMap<String, Integer>();
        ArrayList<String> ordreTraitement = new ArrayList<String>();

        for (Vertex x : g.getListeSommets()) {
            marquer.put(x.getIndex(), false);
            //traiter.put(x.getNom(), 0);
        }
        marquer.put(a.getIndex(), true);
        //int p = 1;
        P.add(a);

        while (P.size() != 0) {
            Vertex x = P.get(P.size() - 1);   //Sommet

            //Boucle qui va changer selon le dernier sommet dans l'ArrayList
            while (g.getListeAdjacence().get(x.getIndex()).size() != 0) {

                boolean yEstMarque = true;  //Si tous les successeurs sont marqués
                for (Edge y : g.getListeAdjacence().get(x.getIndex())) {
                    if (!marquer.get(y.getSommetTerminal())) {
                        marquer.put(y.getSommetTerminal(), true);
                        P.add(g.getListeSommets().get(y.getSommetTerminal()));

                        x = P.get(P.size() - 1);  //Actualiser x avec le dernier sommet de l'ArrayList
                        yEstMarque = false; //Au moins un successeur n'était pas marqué
                        break;
                    }
                }

                //Si tous les successeurs étaient marqués, on sort
                if (yEstMarque) break;
            }

            //traiter.put(x.getNom(), p);
            ordreTraitement.add(P.get(P.size() - 1).getNom());
            P.remove(P.get(P.size() - 1));
            //p += 1;
        }
        return ordreTraitement;
    }

    /**
     * Méthode appelant un parcours en profondeur et formatant son affichage
     * @param a Sommet de départ du parcours
     * @return Une chaîne représentant le parcours effectué
     */
    public String displayParcoursProfondeur(Vertex a)
    {
        ArrayList<String> parcours = Graphe.parcoursProfondeur(this, a);
        String res = "Parcours en Profondeur de " + getName() + " : ";
        for (String sommet : parcours) {
            res += sommet + " ; ";
        }
        return res;
    }


    /**
     * Récupère un élément d'une ArrayList qui possède le même nom que la chaîne passée en paramètre
     * @param nameVertex Nom du sommet à chercher
     * @return Le Vertex correspondant
     */
    public Vertex findVertexByName(String nameVertex)
    {
        return this.getListeSommets().stream().filter(vertex -> nameVertex.equals(vertex.getNom())).findFirst().orElse(null);
    }


    /**
     * Méthode commune aux algorithmes Dijkstra et A* de calcul de distances dans un graphe
     * Calcule les plus courtes distances depuis un sommet jusqu'à un autre dans un graphe (ou tous les autres)
     * @param g Le graphe à utiliser
     * @param si Le sommet de départ
     * @param sf Le sommet d'arrivée (si null, calcule selon tous les sommets)
     * @param heuristiques Heuristiques à utiliser (dans le cas A*)
     * @param peres HashMap stockant les pères des sommets dans le parcours
     * @param dijkstra True si algo Dijkstra, false si A*
     * @return Une HashMap de tous les sommets du graphes avec leur distance respective en valeur
     */
    private static HashMap<Integer, Double> algosDistances(Graphe g, Vertex si, Vertex sf, HashMap<String,
            Double> heuristiques, HashMap<Integer, Vertex> peres, boolean dijkstra)
    {
        //Liste des sommets
        ArrayList<Vertex> Z = new ArrayList<>();
        //Distances au Vertex si
        HashMap<Integer, Double> lambda = new HashMap<>();

        for (Vertex v : g.getListeSommets()) {
            //On enregistre le sommet
            Z.add(v);
            //On initialise tous les sommets à l'infini en distance
            lambda.put(v.getIndex(), Double.POSITIVE_INFINITY);
        }
        //On initialise le premier élément à 0
        lambda.put(si.getIndex(), 0d);

        boolean found = false;

        //Tant que Z non vide ou destination non trouvée
        while (Z.size() > 0 && !found) {
            //Initialiser le minimum au premier élément
            Vertex x = Z.get(0);
            int min_index = 0;

            //Récupérer le minimum et son indice
            for (int i = 1; i < Z.size(); i++) {
                //long startTime = System.nanoTime();
                boolean condition;

                //Cas Dijkstra
                if (dijkstra) {
                    condition = lambda.get(Z.get(i).getIndex()) < lambda.get(x.getIndex());
                    //Cas A*
                } else {
                    condition = lambda.get(Z.get(i).getIndex()) + heuristiques.get(Z.get(i).getNom()) < lambda.get(x.getIndex()) + heuristiques.get(x.getNom());
                }
                if (condition) {
                    x = Z.get(i);
                    min_index = i;
                }
            }

            Z.remove(min_index);    //Retirer le minimum

            //Pour tous les successeurs de x
            for (Edge ed : g.listeAdjacence.get(x.getIndex())) {
                int indexSommet = ed.getSommetTerminal();

                //Si i dans Z et meilleure distance trouvée, on met à jour
                if (Z.contains(g.getListeSommets().get(indexSommet))) {
                    double nouvelleVal = lambda.get(x.getIndex()) + ed.getValeurs()[0];

                    if (nouvelleVal < lambda.get(indexSommet)) {
                        lambda.put(indexSommet, nouvelleVal);
                        //On conserve le père
                        peres.put(indexSommet, x);
                    }
                }
            }

            //Si arrivée définie et qu'on l'a atteint, on sort
            if (sf != null && x.getIndex() == sf.getIndex()) found = true;
        }

        return lambda;
    }


    /**
     * Méthode commune aux algorithmes Dijkstra et A* de calcul de distances dans un graphe version SkipList
     * Calcule les plus courtes distances depuis un sommet jusqu'à un autre dans un graphe
     * Une SkipList est utilisée pour optimiser la recherche du minimum dans les algos
     * @param g Le graphe à utiliser
     * @param si Le sommet de départ
     * @param sf Le sommet d'arrivée
     * @param heuristiques Heuristiques à utiliser (dans le cas A*)
     * @param peres HashMap stockant les pères des sommets dans le parcours
     * @param dijkstra True si algo Dijkstra, false si A*
     * @return Une HashMap de tous les sommets du graphes avec leur distance respective en valeur
     */
    public static HashMap<Integer, Double> algosDistancesSkiplist(Graphe g, Vertex si, Vertex sf, HashMap<String,
            Double> heuristiques, HashMap<Integer, Vertex> peres, boolean dijkstra)
    {
        //Liste des sommets
        HashMap<Integer, Vertex> Z = new HashMap<>();
        //Distances au Vertex si
        HashMap<Integer, Double> lambda = new HashMap<>();
        //Stocker les éléments triés par distance pour accès au minimum plus facile
        SkipList<VertexDistance, Vertex> Zskip = new SkipList<>();

        //On initialise tous les autres sommets à l'infini en distance
        for (Vertex v : g.getListeSommets()) {
            //On enregistre le sommet
            Z.put(v.getIndex(), v);
            //On initialise tous les sommets à l'infini en distance
            lambda.put(v.getIndex(), Double.POSITIVE_INFINITY);
        }
        //On initialise le premier élément à 0
        Zskip.add(new VertexDistance(si.getIndex(), 0d), si);
        lambda.put(si.getIndex(), 0d);

        boolean found = false;

        //Tant que Z non vide ou destination non trouvée
        while (Z.size() > 0 && !found) {
            //Récupération du premier élément de la SkipList
            // => minimum car la liste est ordonnée automatiquement
            SkipList.Node<VertexDistance, Vertex> minNode = Zskip.findNode(Zskip.iterator().next());
            //System.out.println(Zskip.size());
            //System.out.println(minNode);
            VertexDistance minIndexDistance = minNode.getKey();
            Vertex minVertex = minNode.getValue();

            //Retirer le minimum
            Z.remove(minIndexDistance.getId());
            Zskip.remove(minIndexDistance);

            //Pour tous les successeurs de x
            for (Edge ed : g.listeAdjacence.get(minVertex.getIndex())) {
                int indexSommet = ed.getSommetTerminal();

                //Si i dans Z et meilleure distance trouvée, on met à jour
                if (Z.containsKey(indexSommet)) {
                    double nouvelleVal = lambda.get(minVertex.getIndex()) + ed.getValeurs()[0];

                    if (nouvelleVal < lambda.get(indexSommet)) {
                        lambda.put(indexSommet, nouvelleVal);
                        //On conserve le père
                        peres.put(indexSommet, minVertex);

                        //On ajoute l'élément et sa distance dans la SkipList
                        VertexDistance vd;
                        Vertex newV = g.getListeSommets().get(indexSommet);
                        //Cas Dijkstra
                        if (dijkstra) {
                            vd = new VertexDistance(indexSommet, nouvelleVal);
                            //Cas A*
                        } else {
                            vd = new VertexDistance(indexSommet, nouvelleVal + heuristiques.get(newV.getNom()));
                        }
                        Zskip.add(vd, newV);
                    }
                }
            }

            //Si arrivée définie et qu'on l'a atteint, on sort
            if (sf != null && minVertex.getIndex() == sf.getIndex()) found = true;
        }

        return lambda;
    }

    /**
     * Méthode appelant l'algo Dijkstra ou A* (version classique ou SkipList)
     * et formate son affichage
     * @param skiplist True si version SkipList, false sinon
     * @param depart Nom du sommet de départ
     * @param destination Nom du sommet d'arrivée
     * @param heuristiques Heuristiques à utiliser (dans le cas A*)
     * @param dijkstra True si algo Dijkstra, false si A*
     * @return Une chaîne représentant le résultat de l'algo
     */
    public String displayAlgosDistances(boolean skiplist, String depart, String destination, HashMap<String,
            Double> heuristiques, boolean dijkstra)
    {
        String nomAlgo = (dijkstra) ? "Dijkstra" : "A*";
        String res = "--- " + nomAlgo + " (" + this.getName() + ") ---\nDistance " + depart + " - " + destination + " : ";
        Vertex vertexDepart = findVertexByName(depart);
        if (Objects.isNull(vertexDepart)) return "Ville de départ inconnue";
        Vertex vertexDestination = findVertexByName(destination);
        if (Objects.isNull(vertexDestination)) return "Ville d'arrivée inconnue";

        if (this.getNbValeursParSommet() == 2) {
            //HashMap pour récupérer les pères des sommets
            HashMap<Integer, Vertex> peres = new HashMap<Integer, Vertex>();
            HashMap<Integer, Double> map;

            //Exécution de l'algo et calcul du temps d'exécution
            long startTime = System.currentTimeMillis();
            if (skiplist) {
                map = algosDistancesSkiplist(this, vertexDepart, vertexDestination, heuristiques, peres, dijkstra);
            } else {
                map = algosDistances(this, vertexDepart, vertexDestination, heuristiques, peres, dijkstra);
            }
            long endTime = System.currentTimeMillis();

            res += map.get(vertexDestination.getIndex()) + "\n";

            ArrayList<Vertex> list = formatPathFromParentVertex(vertexDepart, vertexDestination, peres);
            res += "Chemin : "
                    + list.stream().map(Object::toString).collect(Collectors.joining(" > "))
                    + "\n";
            res += "Nombre de villes sur le chemin (Bornes exclues) : " + list.size() + "\n";

            res += "Temps d'exécution : " + (endTime - startTime) + " ms";

        } else {
            res = this.getName() + " n'a pas de valeurs sur ses sommets, algo impossible.\n";
        }
        return res;
    }

    /**
     * Forme le chemin entre deux villes depuis une HashMap de pères successifs
     * @param depart Sommet de départ
     * @param arrive Sommet d'arrivée
     * @param peres HashMap des pères des sommets
     * @return Une ArrayList contenant le chemin entre les deux sommets (bornes non comprises)
     */
    public ArrayList<Vertex> formatPathFromParentVertex(Vertex depart, Vertex arrive, HashMap <Integer, Vertex> peres)
    {
        ArrayList<Vertex> trajet = new ArrayList<Vertex>();
        int ind = arrive.getIndex();
        int indDepart = depart.getIndex();
        //Partir de l'arrivée et remonter les pères des sommets
        while (ind != indDepart)
        {
            Vertex sommet = peres.get(ind);
            //Insérer au début du chemin
            trajet.add(0, sommet);
            ind = sommet.getIndex();
        }
        //Collections.reverse(trajet);
        //Enlever le premier sommet (la destination)
        trajet.remove(0);

        return trajet;
    }

    /**
     * Calcule le chemin entre deux sommets d'après l'algo Dijkstra version SkipList
     * @param depart Sommet de départ
     * @param arrive Sommet d'arrivée
     * @return Une ArrayList contenant le chemin entre les deux sommets (bornes non comprises)
     */
    public ArrayList<Vertex> getPathBetween2Cities(Vertex depart, Vertex arrive)
    {
        HashMap <Integer, Vertex> peres = new HashMap <Integer, Vertex>();
        algosDistancesSkiplist(this, depart, arrive, null, peres, true);
        return formatPathFromParentVertex(depart, arrive, peres);
    }


    /**
     * Récupération de la taille de la population des villes depuis un fichier Excel
     * @param file Chemin d'accès au fichier excel
     */
    public void addNbPopulationFromFile(String file)
    {
        BufferedReader reader;
        String line;

        try {
            reader = new BufferedReader(new FileReader(file));
            int i = 0;
            while ((line = reader.readLine()) != null) {

                //Si ligne numéro 2 ou plus dans fichier Excel
                if (i != 0) {
                    String[] row = line.split(";");
                    Vertex sommet = findVertexByName(row[0]);

                    //Si ville du Excel correspond à une ville du graphe
                    if (!Objects.isNull(sommet)) {
                        //Mettre à jour sa taille de population
                        this.getListeSommets().get(sommet.getIndex()).setNbPopulation(Integer.parseInt(row[2]));
                    }
                }
                i++;
            }
        } catch (Exception e) {
            System.out.println("Erreur lecture fichier excel : " + e);
        }
    }

    /**
     * Affiche la taille de la population de chaque ville disponible
     */
    public void displayNbPopulationByCity()
    {
        String res;
        for (Vertex v : this.getListeSommets()) {
            res = "[" + v.getNom() + "] => " + v.getNbPopulation() + " habitant(s)";

            if (v.getNbPopulation() < 0) {
                res = "La ville [" + v.getNom() + "] n'est pas répertoriée";
            }
            System.out.println(res);

            try {
                Thread.sleep(500);

            } catch (Exception e) {
                System.out.println("Erreur temporisation" + e);
            }
        }
    }

    /**
     * Teste si la ville est dans l'ensemble connexe du graphe
     * @param nomVille Nom de la ville à tester
     * @return True si la ville n'est pas dans l'ensemble connexe, false sinon
     */
    public boolean isVilleNonConnexe(String nomVille)
    {
    	return villesNonConnexes.contains(nomVille);
    }

    /**
     * Vérifie si un sommet est atteignable depuis un autre sommet
     * @param pere Sommet de départ
     * @param fils Sommet d'arrivée
     * @return True si le fils est atteignable, false sinon
     */
    public boolean isSonOf(Vertex pere, Vertex fils)
    {
        for(Edge e : this.getListeAdjacence().get(pere.getIndex()))
        {
            if(e.getSommetTerminal() == fils.getIndex()) return true;
        }
        return false;
    }

    /**
     * Evalue la valeur sur l'arc entre deux sommet s'il existe
     * @param pere Sommet de départ
     * @param fils Sommet d'arrivée
     * @return La valeur sur l'arc entre deux sommet s'il existe, l'infini sinon
     */
    public double getValueBetweenVertex(Vertex pere, Vertex fils)
    {
        for(Edge e : this.getListeAdjacence().get(pere.getIndex()))
        {
            if(e.getSommetTerminal() == fils.getIndex()) {
                return e.getValeurs()[0];
            }
        }
        return Double.POSITIVE_INFINITY;
    }

    /**
     * Recupère la ville minimisant la distance à chacune des villes supérieures à un certain seuil de population
     * @param nbPopuMin Seuil de population
     * @return Le sommet remplissant les conditions
     */
    public Vertex bestCityDistantFromEveryOther(int nbPopuMin)
    {
        ArrayList<Vertex> bestCities = new ArrayList<Vertex>();

        HashMap<Integer, HashMap<Integer, Double>> allDijkstras = new HashMap<Integer, HashMap<Integer, Double>>();
        HashMap<Integer, HashMap<Integer, Vertex>> allPeres = new HashMap<Integer, HashMap<Integer, Vertex>>();

        //Récupérer les villes dont la taille est supérieure à nbPopuMin
        for(Vertex v : this.getListeSommets()) 
        {
            if(v.getNbPopulation() >= nbPopuMin) 
            {
                bestCities.add(v);

                //On effectue Dijkstra en avance et on sauvegarde le résultat
                allPeres.put(v.getIndex(), new HashMap<Integer, Vertex>());
                allDijkstras.put(
                        v.getIndex(), algosDistances(
                                this, v, null, null, allPeres.get(v.getIndex()), true
                        )
                );
            }
        }

        Vertex bestCity = null;
        double distance_total = Double.POSITIVE_INFINITY;
        double distance = 0d;

        for(Vertex depart : this.getListeSommets()) {

            //Si sommet de départ non connexe, on passe
            if (isVilleNonConnexe(depart.getNom())) continue;

            distance = 0d;
            for(Vertex arrive : bestCities) {
                //Si sommet de départ identique à l'arrivée, on passe
                if(depart.getIndex() == arrive.getIndex()) continue;
                //Si sommet d'arrivée non connexe, on passe
                if (isVilleNonConnexe(arrive.getNom())) continue;

                //Rajouter au trajet total chaque distance aux villes du chemin
                ArrayList<Vertex> chemin = formatPathFromParentVertex(arrive, depart, allPeres.get(arrive.getIndex()));
                for (Vertex v : chemin){
                    distance += allDijkstras.get(arrive.getIndex()).get(v.getIndex());
                }
            }

            //Si meilleur trajet, on actualise le trajet total et la ville
            if(distance < distance_total) {
                distance_total = distance;
                bestCity = depart;
            }
        }

        //System.out.println(bestCity.getNom() + " avec une distance de " + distance + " km");
        return bestCity;
    }

    /**
     * Retourne la distance totale entre deux sommets
     * @param depart Sommet de départ
     * @param arrive Sommet d'arrivé
     * @return La somme des distances entre chaque ville intermédiaire
     */
    public double distanceBetweenVertex(Vertex depart, Vertex arrive)
    {
        ArrayList<Vertex> chemin = getPathBetween2Cities(depart, arrive);

        double distance = 0d;
        Vertex precedent = depart;

        //Récupération de la distance entre chaque sommet du chemin
        for(Vertex actuel : chemin) {
            distance += getValueBetweenVertex(precedent, actuel);
            if(distance == Double.POSITIVE_INFINITY) {
                System.out.println("Attention infini entre " + precedent.getNom() + " et " + actuel.getNom());
                break;
            }
            precedent = actuel;
        }

        //Mis à part car au cas où le chemin est nul => distance entre départ et arrivée directement
        distance += getValueBetweenVertex(precedent, arrive);
        if(distance == Double.POSITIVE_INFINITY) {
            System.out.println("Attention infini entre " + precedent.getNom() + " et " + arrive.getNom());
        }
        return distance;
    }



    /******************************************************************/


    /**
     * Détermine le chemin entre les villes dépassant un certain seuil de population
     * permettant de les visiter l'une après l'autre, sans y passer deux fois et en revenant
     * à la ville de départ
     * @param nbPopuMin Seuil de population
     * @return L'ArrayList contanant le chemin entre les villes
     */
    public ArrayList<Vertex> circularPathBetweenBestCities(int nbPopuMin)
    {
        ArrayList<Vertex> bestCities = new ArrayList<Vertex>();
        ArrayList<Vertex> copie_bestCities = new ArrayList<Vertex>();
        ArrayList<Vertex> way = new ArrayList<Vertex>();
        double sous_distance = 0d;
        double distance_cumule = 0d;
        double distance_min = Double.POSITIVE_INFINITY;
        for(Vertex v : this.getListeSommets())
        {
            if(v.getNbPopulation() >= nbPopuMin)
            {
                bestCities.add(v);
                copie_bestCities.add(v);
            }
        }
        HashMap<ArrayList<Vertex>, Double> itineraire = new HashMap<ArrayList<Vertex>, Double>();
        int indice_chemin = 0;
        for(Vertex ville_de_depart : bestCities)
        {
            way.add(ville_de_depart);
            Vertex precedent = ville_de_depart;
            copie_bestCities.remove(ville_de_depart);
            Vertex meilleur_ville_suivante = null;
            while(copie_bestCities.size() > 0)
            {
                for(Vertex suivant : copie_bestCities)
                {

                    sous_distance = distanceBetweenVertex(precedent, suivant);
                    if(sous_distance < distance_min)
                    {
                        distance_min = sous_distance;
                        meilleur_ville_suivante = suivant;
                    }
                }
                distance_cumule += distance_min;
                copie_bestCities.remove(meilleur_ville_suivante);
                distance_min = Double.POSITIVE_INFINITY;
                precedent = meilleur_ville_suivante;
                way.add(meilleur_ville_suivante);
            }
            distance_cumule += distanceBetweenVertex(precedent, ville_de_depart);
            ArrayList<Vertex> copie_way = new ArrayList<Vertex>();
            copie_way.addAll(way);
            itineraire.put(copie_way, distance_cumule);
            distance_cumule = 0d;
            way.clear();
            copie_bestCities.clear();
            copie_bestCities.addAll(bestCities);
        }
        ArrayList<Vertex> way_final = new ArrayList<Vertex>();
        double distanceMin = Double.POSITIVE_INFINITY;
        for(Map.Entry<ArrayList<Vertex>, Double> entry : itineraire.entrySet())
        {
            if(entry.getValue() < distanceMin)
            {
                distanceMin = entry.getValue();
                way_final = entry.getKey();
            }
        }
        System.out.println("distance_total : " + distanceMin);
        return way_final;
    }




    public void test(int x, ArrayList<Vertex> trajet)
    {
    	/*
        ArrayList<Vertex> v = new ArrayList<Vertex>();
        
        switch(x)
        {
	        case 200000:
	            v.add(findVertexByName("paris"));
	            v.add(findVertexByName("lille"));
	            v.add(findVertexByName("strasbourg"));
	            v.add(findVertexByName("lyon"));
	            v.add(findVertexByName("montpellier"));
	            v.add(findVertexByName("marseille"));
	            v.add(findVertexByName("nice"));
	            v.add(findVertexByName("toulouse"));
	            v.add(findVertexByName("bordeaux"));
	            v.add(findVertexByName("nantes"));
	            v.add(findVertexByName("rennes"));
	        break;
	        
	        case 150000:
	        	v.add(findVertexByName("toulon"));
	            v.add(findVertexByName("marseille"));
	            v.add(findVertexByName("montpellier"));
	            v.add(findVertexByName("toulouse"));
	            v.add(findVertexByName("bordeaux"));
	            v.add(findVertexByName("nantes"));
	            v.add(findVertexByName("rennes"));
	            v.add(findVertexByName("havre"));
	            v.add(findVertexByName("paris"));
	            v.add(findVertexByName("reims"));
	            v.add(findVertexByName("lille"));
	            v.add(findVertexByName("strasbourg"));
	            v.add(findVertexByName("dijon"));
	            v.add(findVertexByName("lyon"));
	            v.add(findVertexByName("saint-etienne"));
	            v.add(findVertexByName("grenoble"));
	            v.add(findVertexByName("nice"));
	        break;
	        	
	        case 100000:
	        	v.add(findVertexByName("argenteuil"));
	            v.add(findVertexByName("saint-denis-93"));
	            v.add(findVertexByName("paris"));
	            v.add(findVertexByName("montreuil-93"));
	            v.add(findVertexByName("boulogne-billancourt"));
	            v.add(findVertexByName("orleans"));
	            v.add(findVertexByName("tours"));
	            v.add(findVertexByName("le-mans"));
	            v.add(findVertexByName("angers"));
	            v.add(findVertexByName("nantes"));
	            v.add(findVertexByName("rennes"));
	            v.add(findVertexByName("caen"));
	            v.add(findVertexByName("havre"));
	            v.add(findVertexByName("rouen"));
	            v.add(findVertexByName("amiens"));
	            v.add(findVertexByName("lille"));
	            v.add(findVertexByName("reims"));
	            v.add(findVertexByName("nancy"));
	            v.add(findVertexByName("metz"));
	            v.add(findVertexByName("strasbourg"));
	            v.add(findVertexByName("mulhouse"));
	            v.add(findVertexByName("besancon"));
	            v.add(findVertexByName("dijon"));
	            v.add(findVertexByName("villeurbanne"));
	            v.add(findVertexByName("lyon"));
	            v.add(findVertexByName("saint-etienne"));
	            v.add(findVertexByName("grenoble"));
	            v.add(findVertexByName("nimes"));
	            v.add(findVertexByName("montpellier"));
	            v.add(findVertexByName("aix-en-provence"));
	            v.add(findVertexByName("marseille"));
	            v.add(findVertexByName("toulon"));
	            v.add(findVertexByName("nice"));
	            v.add(findVertexByName("perpignan"));
	            v.add(findVertexByName("toulouse"));
	            v.add(findVertexByName("bordeaux"));
	            v.add(findVertexByName("limoges"));
	            v.add(findVertexByName("clermont-ferrand"));
	            v.add(findVertexByName("brest"));
	        break;
	        	
	        default :
	        	
	        break;
        }
		*/
        Vertex precedent = null;
        boolean check = true;
        double distance = 0d;
        for(Vertex suivant : trajet)
        {
            if(check)
            {
                check = false;
                precedent = suivant;
                continue;
            }
            distance += distanceBetweenVertex(precedent, suivant);
            precedent = suivant;
        }
        distance += distanceBetweenVertex(precedent, trajet.get(0));
        System.out.println("DISTANCE test : " + distance);
    }

    public void testOld()
    {
    	/*
    	ArrayList<Vertex> b = new ArrayList<Vertex>();
    	b.add(findVertexByName("nice"));
    	b.add(findVertexByName("marseille"));
    	b.add(findVertexByName("montpellier"));
    	b.add(findVertexByName("toulouse"));
    	b.add(findVertexByName("bordeaux"));
    	b.add(findVertexByName("nantes"));
    	b.add(findVertexByName("rennes"));
    	b.add(findVertexByName("paris"));
    	b.add(findVertexByName("lille"));
    	b.add(findVertexByName("strasbourg"));
    	b.add(findVertexByName("lyon"));

    	boolean check = true;
    	Vertex a = null;
    	double distance = 0d;
    	for(Vertex v : b)
    	{
    		if(check)
    		{
    			check = false;
    			a = v;
    			continue;
    		}
    		distance += distanceBetweenVertex(a, v);
    		a = v;
    	}
    	distance += distanceBetweenVertex(a, b.get(0));
    	System.out.println("distance : " + distance);
    	*/
        Vertex pre = findVertexByName("villeurbanne");
        Vertex sui = findVertexByName("lyon");
        System.out.println("Test et valeur: " + isSonOf(sui, pre) + " " + getValueBetweenVertex(sui, pre));
    }


    public void testAdvanced(ArrayList<Vertex> trajet)
    {
    	Vertex precedent = trajet.get(0);
    	boolean check = true;
    	for(Vertex suivant : trajet)
    	{
    		if(precedent.equals(trajet))
    		{
    			
    		}
    		System.out.println(precedent.getNom().toUpperCase());
    		ArrayList<Vertex> under_way = getPathBetween2Cities(precedent, suivant);
    		for(Vertex ville : under_way)
    		{
    			System.out.println(ville.getNom());
    		}
    		precedent = suivant;
    	}
    }





    /**
     * récupérer un graphe depuis un fichier
     *
     * @param file
     */
    public void getGrapheFromFile(String file) {

        try {
            List<String> strfile = Files.readAllLines((Paths.get(file)));

            //Booléens de détection
            boolean isParameters = false, isVertices = false, isEdges = false;

            //Récupération depuis le fichier
            for (int i = 0; i < strfile.size(); i++) {// String ligne : strfile){

                //Récupérer la ligne sous forme de tableau
                String[] ligneTab = strfile.get(i).split("[ ]*:[ ]*");

                /**
                 * Partie Name
                 */
                if (ligneTab[0].equals("Name")) {
                    this.setName(ligneTab[1]);

                    /**
                     * Partie Oriented
                     */
                } else if (ligneTab[0].equals("Oriented")) {

                    this.setOriente(ligneTab[1].equals("true"));
                    //this.setOriente( false );

                    /**
                     * Partie détection Parameters
                     */
                } else if (ligneTab[0].startsWith("Parameters")) {
                    //String[] params = ligneTab[0].substring(ligneTab[0].indexOf("[") + 1, ligneTab[0].indexOf("]")).split(" ");
                    isParameters = true;

                    /**
                     * Partie Parameters en cours
                     */
                } else if (isParameters) {
                    String[] params = ligneTab[0].split(" ");

                    this.setNbSommets(Integer.parseInt(params[0]));
                    this.initializeListeAdjacence();
                    this.initializeListesDegres();

                    this.setNbValeursParSommet(Integer.parseInt(params[1]));
                    this.setNbArcs(Integer.parseInt(params[2]));
                    this.setNbValeursParArc(Integer.parseInt(params[3]));

                    isParameters = false;

                    /**
                     * Partie détection Vertices
                     */
                } else if (ligneTab[0].startsWith("Vertices")) {
                    isVertices = true;
                    continue;

                    /**
                     * Partie détection Edges
                     */
                } else if (ligneTab[0].startsWith("Edges")) {
                    isVertices = false;
                    isEdges = true;
                    continue;
                }

                /**
                 * Partie Vertices en cours
                 */
                if (isVertices) {
                    String[] verticeLine = ligneTab[0].split(" ");

                    Vertex new_vertex;
                    //S'il y a des valeurs sur le sommet
                    if (nbValeursParSommet > 0) {
                        double[] tab = new double[nbValeursParSommet];
                        for (int k = 2; k < verticeLine.length; k++) {
                            tab[k - 2] = Double.parseDouble(verticeLine[k]);
                        }
                        new_vertex = new Vertex(Integer.parseInt(verticeLine[0]), verticeLine[1], tab);

                        //S'il n'y a pas de valeurs sur le sommet
                    } else {
                        new_vertex = new Vertex(Integer.parseInt(verticeLine[0]), verticeLine[1]);
                    }
                    this.getListeSommets().add(new_vertex);

                    /**
                     * Partie Edges en cours
                     */
                } 
                else if (isEdges) {
                    String[] edgeLine = ligneTab[0].split(" ");

                    int currentEdge = Integer.parseInt(edgeLine[0]);
                    int currentTerminal = Integer.parseInt(edgeLine[1]);

                    if (edgeLine.length > 1) {
                        LinkedList<Edge> list = this.getListeAdjacence().get(currentEdge);

                        //S'il y a des valeurs, les prendre sinon tableau vide
                        String[] edgeVal = (edgeLine.length > 2) ? Arrays.copyOfRange(edgeLine, 2, edgeLine.length) : new String[0];

                        list.add(new Edge(
                                currentTerminal,
                                Arrays.stream(edgeVal)
                                        .mapToDouble(Double::parseDouble)
                                        .toArray()
                        ));

                        //Si non orienté, inverser l'arc et l'ajouter
                        if (!this.isOriente()) {
                            LinkedList<Edge> listInverse = this.getListeAdjacence().get(currentTerminal);

                            listInverse.add(new Edge(
                                    currentEdge,
                                    Arrays.stream(edgeVal)
                                            .mapToDouble(Double::parseDouble)
                                            .toArray()
                            ));
                        }

                        //Actualiser les degrés
                        this.getListeDegresExterieurs()[currentEdge] += 1;       //Degrés extérieur du sommet initial
                        this.getListeDegresInterieurs()[currentTerminal] += 1;   //Degrés intérieur du sommet terminal
                    }
                }
            }
            isEdges = false;

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    
    public String toString() {
        return "Graphe : " + getName() + "\n" +
                "Orienté : " + isOriente() + "\n" +
                "nbSommets : " + getNbSommets() + "\n" +
                "nbArcs : " + getNbArcs() + "\n" +
                "sommets : " + getListeSommetsToString() + "\n" +
                "arcs : " + getListeAdjacenceToString() //+ "\n"
                ;
    }

}
