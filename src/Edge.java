package src;

public class Edge {

    private final int sommetTerminal;
    private double[] valeurs;

    public Edge(int sommetTerminal, double[] valeurs)
    {
        this.sommetTerminal = sommetTerminal;
        this.valeurs = valeurs;
    }

    public int getSommetTerminal()
    {
        return sommetTerminal;
    }

    public double[] getValeurs()
    {
        return valeurs;
    }

    /**
     * Formate l'affichage des valeurs de l'arc
     * @return La chaîne formatée
     */
    public String getValeursToString()
    {
        String res = "";
        if (getValeurs().length > 0) {
            res += "(";
            for (double val : getValeurs()) {
                res += val + ", ";
            }
            res = res.substring(0, res.length() - 2) + ") ";
        } else {
            res += " ";
        }
        return res;
    }

    public void setValeurs(double[] valeurs)
    {
        this.valeurs = valeurs;
    }

    @Override
    public String toString()
    {
        return "Edge -> " + sommetTerminal + " "
                + getValeursToString();
    }
}
