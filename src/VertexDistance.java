package src;

/**
 * Classe servant de clé lors de l'insertion d'un élément dans une SkipList
 * Elle contient la distance ainsi que l'id, pour avoir une valeur de comparaison
 * si les distances sont égales.
 */
public class VertexDistance implements Comparable<VertexDistance> {

    private int id;             //Index du Vertex
    private double distance;    //Distance du Vertex

    public VertexDistance(int id, double distance)
    {
        this.id = id;
        this.distance = distance;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public double getDistance()
    {
        return distance;
    }

    public void setDistance(double distance)
    {
        this.distance = distance;
    }


    /**
     * Méthode de comparaison d'entiers pour compareTo
     */
    public static int compare(int x, int y)
    {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }

    @Override
    /**
     * Compare les VertexDistance selon leur attribut distance,
     * En cas d'égalité, une comparaison sur l'id (unique) est effectuée.
     */
    public int compareTo(VertexDistance o)
    {
        if (getDistance() < o.getDistance()) {
            return -1;
        } else if (getDistance() > o.getDistance()) {
            return 1;
        } else {
            return compare(getId(), o.getId());
        }
    }
}
