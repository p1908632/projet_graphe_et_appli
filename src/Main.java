package src;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
    	
        String folder = "files/";
        //System.out.println("CWD: " + System.getProperty("user.dir"));
        ArrayList<String> fileNames = new ArrayList<String>();
        fileNames.add(folder + "Tests/Cours_Parcours.tgoGraph");        //0
        fileNames.add(folder + "Tests/Cours_Representation.tgoGraph");  //1
        fileNames.add(folder + "Tests/Graphe_01.tgoGraph");             //2
        fileNames.add(folder + "Tests/Graphe_02.tgoGraph");             //3
        fileNames.add(folder + "exo.tgoGraph");                         //4
        fileNames.add(folder + "CommunesFrance_5000coord.tgoGraph");    //5
        fileNames.add(folder + "CommunesFrance_10000coord.tgoGraph");   //6
        String ExcelFile = folder + "CommunesFrance.csv";

        Graphe graphe1 = new Graphe(fileNames.get(5));
        
        //String depart = "toulouse", arrivee = "paris";
        String depart = "replonges", arrivee = "nice";

        //Calcul des heuristiques utilisée dans le programme
        HashMap<String, Double> heuristiques = graphe1.initializeHeuristiques(arrivee);

/*
        System.out.println("==================== Test des Algos ==================");
        //Affichage du graphe
        //System.out.println(graphe1);
        System.out.println("=======");
        //Dijkstra Total (sans sommet de destination)
        //System.out.println( graphe1.displayDijkstra(graphe1.getListeSommets().get(0)) );
        System.out.println("=======");
        //Dijkstra
        System.out.println(graphe1.displayAlgosDistances(false, depart, arrivee, heuristiques, true));
        System.out.println("=======");
        //A*
        System.out.println(graphe1.displayAlgosDistances(false, depart, arrivee, heuristiques, false));
        System.out.println("=======");
        //Dijkstra - SkipList
        System.out.println(graphe1.displayAlgosDistances(true, depart, arrivee, heuristiques, true));
        System.out.println("=======");
        //A* - SkipList
        System.out.println(graphe1.displayAlgosDistances(true, depart, arrivee, heuristiques, false));
        System.out.println("=======");
*/


        //System.out.println("==================== Extraction depuis le ficher Excel ==================");
        long startTime = System.currentTimeMillis();
        graphe1.addNbPopulationFromFile(ExcelFile);
        System.out.println("Durée de la récupération : " + print_time(System.currentTimeMillis() - startTime) + "\n");
		

        System.out.println("==================== Meilleure ville et Meilleur chemin ==================");
        int[] seuils = {200000, 150000, 100000};

        startTime = System.currentTimeMillis();
        //System.out.println("Meilleure ville : " + graphe1.bestCityDistantFromEveryOther(seuils[0]).getNom());
        System.out.println("Durée de la méthode : " + print_time(System.currentTimeMillis() - startTime) + "\n");

        System.out.println("=======");

        int x = seuils[2];
        
        startTime = System.currentTimeMillis();
        ArrayList<Vertex> trajet = graphe1.circularPathBetweenBestCities(x);
        graphe1.test(x, trajet);
        System.out.println("Durée de la méthode : " + print_time(System.currentTimeMillis() - startTime) + "\n");
        System.out.println("Pour x = " + x + " le trajet comporte " + trajet.size() + " grandes villes");
        for(Vertex v : trajet)
        {
        	System.out.print(v.getNom() + " ; ");
        }
        System.out.println("\n");
		

        System.out.println("==================== Fin du Programme ==================");
    }

    
    public static String print_time(long miliseconde)
    {
    	String res;
		int seconde = (int) miliseconde/1000;
		int minute = seconde/60;
		int heure = minute/60;
		seconde = (int)(miliseconde/1000 - (long)(heure*3600 + minute*60));
		if(heure == 0)
		{
			if(minute == 0)
			{
				if(seconde == 0)
				{
					res = ("0." + adapt_ms(miliseconde));
				}
				else
				{
					res = (seconde + "." + adapt_ms(miliseconde) + "s");
				}
			}
			else
			{
				res = (minute + "m " + seconde + "." + adapt_ms(miliseconde) + "s");
			}
		}
		else
		{
			res = (heure + "h " + minute + "m " + seconde + "." + adapt_ms(miliseconde) + "s");
		}
		return res;
    }
    public static int adapt_ms(long ms)
    {
    	int retour;
    	if(ms >= 1000)
    	{
    		retour = (int)ms/1000;
    		retour = (int)(ms) - retour*1000;
    		return retour;
    	}
    	return (int)(ms);
    }

}
