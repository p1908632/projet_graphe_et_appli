package src;

public class Vertex {

    private int index;
    private String nom;
    private double[] valeursSommet;
    private int nbPopulation = -1;

    //Constructeur sans valeurs sur sommet
    public Vertex(int index, String nom)
    {
        this.index = index;
        this.nom = nom;
    }

    //Constructeur avec valeurs sur sommet
    public Vertex(int index, String nom, double[] valeursSommet)
    {
        this.index = index;
        this.nom = nom;
        this.valeursSommet = valeursSommet;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public String getNom()
    {
        return nom;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public double[] getValeursSommet()
    {
        return valeursSommet;
    }

    /**
     * Formate l'affichage des valeurs du sommet
     * @return La chaîne formatée
     */
    public String getValeursSommetToString()
    {
        String res = "(";

        for (int j = 0; j < this.getValeursSommet().length; j++) {
            res += this.getValeursSommet()[j];
            if (j < this.getValeursSommet().length - 1) res += " ; ";
        }
        res += ") ";

        return res;
    }

    public void setValeursSommet(double[] valeursurSommet)
    {
        this.valeursSommet = valeursurSommet;
    }

    public int getNbPopulation()
    {
        return nbPopulation;
    }

    public void setNbPopulation(int nbPopulation)
    {
        this.nbPopulation = nbPopulation;
    }

    @Override
    public String toString()
    {
        return "[" + getIndex() + "] " + getNom() + " "
                + getValeursSommetToString();
    }
}
